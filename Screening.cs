﻿// ============================================================
// Student Number : S10222764, S10227932
// Student Name : Xiao Yaojie, Shuqri Bin Shaifuddin
// Module Group : T04
// ============================================================

using System;

namespace prg2_t04_team1
{
    public class Screening
    {
        // Properties
        public int ScreeningNo { get; set; }
        public DateTime ScreeningDateTime { get; set; }
        public string ScreeningType { get; set; }
        public int SeatsRemaining { get; set; }
        public Cinema Cinema { get; set; }
        public Movie Movie { get; set; }
        
        // Constructors
        public Screening(){}
        public Screening(int sn, DateTime sdt, string st, Cinema c, Movie m)
        {
            ScreeningNo = sn;
            ScreeningDateTime = sdt;
            ScreeningType = st;
            Cinema = c;
            Movie = m;
        }

        // Methods
        public override string ToString()
        {
            return "ScreeningNo: " + ScreeningNo + "\tScreeningDateTime: " + ScreeningDateTime + "\tScreeningType: " + ScreeningType + 
                "\tSeatsRemaining: " + SeatsRemaining + "\tCinema: " + Cinema + "\tMovie: " + Movie;
        }
    }
}