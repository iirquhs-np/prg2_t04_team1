﻿// ============================================================
// Student Number : S10222764, S10227932
// Student Name : Xiao Yaojie, Shuqri Bin Shaifuddin
// Module Group : T04
// ============================================================

using System;
using System.Collections.Generic;

namespace prg2_t04_team1
{
    public class Movie
    {
        // Properties
        public string Title { get; set; }
        public int Duration { get; set; }
        public string Classification { get; set; }
        public DateTime OpeningDate { get; set; }
        public List<String> GenreList { get; set; }
        public List<Screening> ScreeningList { get; set; }
        
        // Constructors
        public Movie(){}

        public Movie(string t, int d, string c, DateTime od, List<String> gnList)
        {
            Title = t;
            Duration = d;
            Classification = c;
            OpeningDate = od;
            GenreList = gnList;
            ScreeningList = new List<Screening>();
        }

        // Methods
        public void AddGenre(string gn)
        {
            GenreList.Add(gn);
        }

        public void AddScreening(Screening s)
        {
            ScreeningList.Add(s);
        }

        public override string ToString()
        {
            return "Title: " + Title + "\tDuration: " + Duration +
                   "\tClassificiation: " + Classification + "\tOpeningDate: " + OpeningDate;
        }
    }
}