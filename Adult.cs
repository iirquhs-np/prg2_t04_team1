﻿// ============================================================
// Student Number : S10222764, S10227932
// Student Name : Xiao Yaojie, Shuqri Bin Shaifuddin
// Module Group : T04
// ============================================================

namespace prg2_t04_team1
{
    public class Adult : Ticket
    {
        // Properties
        public bool PopcornOffer { get; set; }
        
        // Constructors
        public Adult(){}
        public Adult(Screening s, string sn, bool po) : base(s, sn)
        {
            PopcornOffer = po;
        }

        // Methods
        public override double CalculatePrice()
        {
            int dayOfWeek = (int) Screening.ScreeningDateTime.DayOfWeek;
            string screeningType = Screening.ScreeningType;
            double price;

            if (screeningType == "3D") // If 3D movie
            {
                if (dayOfWeek >= 1 && dayOfWeek <= 4) // If during Monday to Thursday
                {
                    price = 11.00;
                }
                else // If during Friday to Sunday
                {
                    price = 14.00;
                }
            }
            else // If 2D movie
            {
                if (dayOfWeek >= 1 && dayOfWeek <= 4) // If during Monday to Thursday
                {
                    price = 8.50;
                }
                else // If during Friday to Sunday
                {
                    price = 12.50;
                }
            }
            if (PopcornOffer)
            {
                return price += 3.00; // If purchased popcorn offer
            }
            else
            {
                return price; // If did not purchase popcorn offer
            }
        }
        public override string ToString()
        {
            return base.ToString() + "\tPopcornOffer: " + PopcornOffer;
        }
    }
}