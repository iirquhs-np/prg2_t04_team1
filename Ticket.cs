﻿// ============================================================
// Student Number : S10222764, S10227932
// Student Name : Xiao Yaojie, Shuqri Bin Shaifuddin
// Module Group : T04
// ============================================================

namespace prg2_t04_team1
{
    public abstract class Ticket
    {
        // Properties
        public Screening Screening { get; set; }
        public string SeatNo { get; set; }
        
        // Constructors
        public Ticket(){}
        public Ticket(Screening s, string sn)
        {
            Screening = s;
            SeatNo = sn;
        }

        // Methods
        public abstract double CalculatePrice();

        public override string ToString()
        {
            return "Screening: " + Screening + "\tSeatNo: " + SeatNo;
        }
    }
}