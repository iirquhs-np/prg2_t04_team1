﻿// ============================================================
// Student Number : S10222764, S10227932
// Student Name : Xiao Yaojie, Shuqri Bin Shaifuddin
// Module Group : T04
// ============================================================

using System;

namespace prg2_t04_team1
{
    public class Student : Ticket
    {
        // Properties
        public string LevelOfStudy { get; set; }
        
        // Constructors
        public Student(){}

        public Student(Screening s, string sn, string los) : base(s, sn)
        {
            LevelOfStudy = los;
        }

        // Methods
        public override double CalculatePrice()
        { 
            int dayOfWeek = (int) Screening.ScreeningDateTime.DayOfWeek;
            string screeningType = Screening.ScreeningType;

            if ((DateTime.Now - Screening.Movie.OpeningDate).TotalDays <= 7) // If during first 7 days of movie opening date
            {
                if (screeningType == "3D") // If 3D movie
                {
                    if (dayOfWeek >= 1 && dayOfWeek <= 4) // If during Monday to Thursday
                    {
                        return 11.00;
                    }
                    else // If during Friday to Sunday
                    {
                        return 14.00;
                    }
                }
                else // If 2D movie
                {
                    if (dayOfWeek >= 1 && dayOfWeek <= 4) // If during Monday to Thursday
                    {
                        return 8.50;
                    }
                    else // If during Friday to Sunday
                    {
                        return 12.50;
                    }
                }
            }
            else // If after first 7 days of movie opening date
            {
                if (screeningType == "3D") // If 3D movie
                {
                    if (dayOfWeek >= 1 && dayOfWeek <= 4) // If during Monday to Thursday
                    {
                        return 8.00;
                    }
                    else // If during Friday to Sunday
                    {
                        return 14.00;
                    }
                }
                else // If 2D movie
                {
                    if (dayOfWeek >= 1 && dayOfWeek <= 4) // If during Monday to Thursday
                    {
                        return 7.00;
                    }
                    else // If during Friday to Sunday
                    {
                        return 12.50;
                    }
                }
            }
        }

        public override string ToString()
        {
            return base.ToString() + "\tLevelOfStudy: " + LevelOfStudy;
        }
    }
}