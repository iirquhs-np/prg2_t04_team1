﻿// ============================================================
// Student Number : S10222764, S10227932
// Student Name : Xiao Yaojie, Shuqri Bin Shaifuddin
// Module Group : T04
// ============================================================

using System;

namespace prg2_t04_team1
{
    public class SeniorCitizen : Ticket
    {
        // Properties
        public int YearOfBirth { get; set; }
        
        // Constructors
        public SeniorCitizen(){}
        public SeniorCitizen(Screening s, string sn, int yob) : base(s, sn)
        {
            YearOfBirth = yob;
        }

        // Methods
        public override double CalculatePrice()
        {
            int dayOfWeek = (int) Screening.ScreeningDateTime.DayOfWeek;
            string screeningType = Screening.ScreeningType;
            
            if ((DateTime.Now - Screening.Movie.OpeningDate).TotalDays <= 7) // If during first 7 days of movie opening date
            {
                if (screeningType == "3D") // If 3D movie
                {
                    if (dayOfWeek >= 1 && dayOfWeek <= 4) // If during Monday to Thursday
                    {
                        return 11.00;
                    }
                    else // If during Friday to Sunday
                    {
                        return 14.00;
                    }
                }
                else // If 2D movie
                {
                    if (dayOfWeek >= 1 && dayOfWeek <= 4) // If during Monday to Thursday
                    {
                        return 8.50;
                    }
                    else // If during Friday to Sunday
                    {
                        return 12.50;
                    }
                }
            }
            else // If after first 7 days of movie opening date
            {
                if (screeningType == "3D") // If 3D movie
                {
                    if (dayOfWeek >= 1 && dayOfWeek <= 4) // If during Monday to Thursday
                    {
                        return 6.00;
                    }
                    else // If during Friday to Sunday
                    {
                        return 14.00;
                    }
                }
                else // If 2D movie
                {
                    if (dayOfWeek >= 1 && dayOfWeek <= 4) // If during Monday to Thursday
                    {
                        return 5.00;
                    }
                    else // If during Friday to Sunday
                    {
                        return 12.50;
                    }
                }
            }
        }

        public override string ToString()
        {
            return base.ToString() + "\tYearOfBirth: " + YearOfBirth;
        }
    }
}