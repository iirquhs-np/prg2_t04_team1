﻿// ============================================================
// Student Number : S10222764, S10227932
// Student Name : Xiao Yaojie, Shuqri Bin Shaifuddin
// Module Group : T04
// ============================================================

namespace prg2_t04_team1
{
    public class Cinema
    {
        // Properties
        public string Name { get; set; }
        public int HallNo { get; set; }
        public int Capacity { get; set; }
        
        // Constructors
        public Cinema(){}
        public Cinema(string n, int hn, int c)
        {
            Name = n;
            HallNo = hn;
            Capacity = c;
        }

        // Methods
        public override string ToString()
        {
            return "Name: " + Name + "\tHallNo: " + HallNo + "\tCapacity: " + Capacity;
        }
    }
}