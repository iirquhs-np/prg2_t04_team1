﻿// ============================================================
// Student Number : S10222764, S10227932
// Student Name : Xiao Yaojie, Shuqri Bin Shaifuddin
// Module Group : T04
// ============================================================

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace prg2_t04_team1
{
    class Program
    {
        // ==================================================  MAIN PROGRAM ===================================================
        static void Main(string[] args)
        {
            // FEATURE 1: LOAD MOVIE AND CINEMA DATA
            List<Movie> movieList = new List<Movie>();
            InitMovieData(movieList);
            List<Cinema> cinemaList = new List<Cinema>();
            InitCinemaData(cinemaList);

            // FEATURE 2: LOAD SCREENING DATA
            List<Screening> screeningList = new List<Screening>();
            InitScreeningData(screeningList, cinemaList, movieList);

            // INITIALISE ORDER NUMBER 
            int orderNo = 0;
            List<Order> orderList = new List<Order>();

            // MAIN MENU
            while (true)
            {
                string option = Menu();
                Console.Clear();
                if (option == "1")
                {
                    // FEATURE 3: LIST ALL MOVIES
                    Console.WriteLine("Displaying all movies ...");
                    Console.WriteLine();
                    ListAllMovies(movieList);

                    PressToContinue();
                    Console.Clear();
                }
                else if (option == "2")
                {
                    // FEATURE 4: LIST MOVIE SCREENINGS
                    ListMovieScreenings(movieList, screeningList);

                    PressToContinue();
                    Console.Clear();
                }
                else if (option == "3")
                {
                    // FEATURE 7: ORDER MOVIE TICKETS
                    OrderMovieTickets(movieList, screeningList, orderNo, orderList);

                    PressToContinue();
                    Console.Clear();
                }
                else if (option == "4")
                {
                    // FEATURE 8: CANCEL ORDER OF TICKET
                    CancelMovieTickets(orderList);

                    PressToContinue();
                    Console.Clear();
                }
                else if (option == "5")
                {
                    // ADVANCED FEATURE 3.1: DISPLAY AVAILABLE SEATS OF SCREENING SESSION IN DESCENDING ORDER
                    ScreeningAvailableSeats(screeningList);

                    PressToContinue();
                    Console.Clear();
                }
                else if (option == "6")
                {

                    bool isValid = false;
                    for (int i = 0; i < 3; i++) // Exits if enters more than 3 wrong passwords
                    {
                        Console.Clear();
                        Console.Write("Please enter password: ");
                        string password = Console.ReadLine();
                        if (password == "Pa$$w0rd")
                        {
                            isValid = true;
                            Console.Clear();
                            break;
                        }
                        else
                        {
                            Console.Write("Wrong password entered.");
                            Console.ReadKey();
                        }
                    }

                    if (isValid)
                    {
                        while (true)
                        {
                            string staffOption = StaffMenu();
                            Console.Clear();
                            if (staffOption == "1")
                            {
                                // FEATURE 5: ADD A MOVIE SCREENING SESSION
                                ListAllMovies(movieList);
                                AddScreeningSession(movieList, screeningList, cinemaList);

                                PressToContinue();
                            }
                            else if (staffOption == "2")
                            {
                                // FEATURE 6: DELETE A MOVIE SCREENING SESSION
                                DeleteScreeningSession(screeningList);

                                PressToContinue();

                            }
                            else if (staffOption == "0")
                            {
                                break;
                            }
                            Console.Clear();

                        }
                    }
                    Console.WriteLine("Exiting staff menu.");
                    PressToContinue();
                    Console.Clear();

                }
                else if (option == "0")
                {
                    break;
                }
                else
                {
                    Console.WriteLine("You have not selected a right option.");
                    Console.WriteLine();
                }
            }
        }
        // ===============================================  END OF MAIN PROGRAM ===============================================



        // =====================================================  METHODS =====================================================
        static string Menu()
        {
            Console.WriteLine("================ SINGA CINEPLEXES ================");
            Console.WriteLine("============= MOVIE TICKETING SYSTEM =============");
            Console.WriteLine();
            Console.WriteLine("[1] Display all movies");
            Console.WriteLine("[2] List movie screenings");
            Console.WriteLine("[3] Order a movie ticket");
            Console.WriteLine("[4] Cancel order of movie ticket");
            Console.WriteLine("[5] List available seats of screening sessions in descending order");
            Console.WriteLine();
            Console.WriteLine("[6] Staff");
            Console.WriteLine("[0] Exit");
            Console.Write("Please enter your option here: ");
            return Console.ReadLine();
        }

        static string StaffMenu()
        {
            Console.WriteLine("================ SINGA CINEPLEXES ================");
            Console.WriteLine("=================== STAFF MENU ===================");
            Console.WriteLine();
            Console.WriteLine("[1] Add a movie screening session");
            Console.WriteLine("[2] Delete a movie screening session");
            Console.WriteLine();
            Console.WriteLine("[0] Exit");
            Console.Write("Please enter your option here: ");
            return Console.ReadLine();
        }

        static void PressToContinue()
        {
            Console.Write("Press any key to continue ...");
            Console.ReadKey();
            Console.WriteLine();
            Console.WriteLine();
        }

        // FEATURE 1: LOAD MOVIE DATA
        static void InitMovieData(List<Movie> movieList)
        {
            using (StreamReader sr = new StreamReader("Movie.csv"))
            {
                string line = sr.ReadLine();
                while ((line = sr.ReadLine()) != null)
                {
                    string[] data = line.Split(",");
                    // DATA SPECIFICATIONS
                    // data[0] = Title, data[1] = Duration (mins), data[2] = Genre
                    // data[3] = Classification, data[4] = Opening Date
                    List<String> gnList = new List<String>();
                    string[] gnData = data[2].Split("/");
                    foreach (string genre in gnData)
                    {
                        gnList.Add(genre);
                    }
                    Movie newMovie = new Movie(data[0], Convert.ToInt32(data[1]), data[3], Convert.ToDateTime(data[4]), gnList);
                    movieList.Add(newMovie);
                }
            }
        }

        // FEATURE 1: LOAD CINEMA DATA
        static void InitCinemaData(List<Cinema> cinemaList)
        {
            using (StreamReader sr = new StreamReader("Cinema.csv"))
            {
                string line = sr.ReadLine();
                while ((line = sr.ReadLine()) != null)
                {
                    string[] data = line.Split(",");
                    // DATA SPECIFICATIONS
                    // data[0] = Name, data[1] = Hall Number, data[2] = Capacity
                    Cinema newCinema = new Cinema(data[0], Convert.ToInt32(data[1]), Convert.ToInt32(data[2]));
                    cinemaList.Add(newCinema);
                }
            }
        }

        // FEATURE 2: LOAD SCREENING DATA
        static void InitScreeningData(List<Screening> screeningList, List<Cinema> cinemaList, List<Movie> movieList)
        {
            using (StreamReader sr = new StreamReader("Screening.csv"))
            {
                string line = sr.ReadLine();
                int i = 1001;
                while ((line = sr.ReadLine()) != null)
                {
                    string[] data = line.Split(",");
                    // DATA SPECIFICATIONS
                    // data[0] = Date Time, data[1] = Screening Type, data[2] = Cinema Name
                    // data[3] = Hall Number, data[4] = Movie Title
                    Cinema cData = new Cinema();
                    int seats = 0;
                    foreach (Cinema c in cinemaList)
                    {
                        if (c.Name == data[2] && c.HallNo == Convert.ToInt32(data[3]))
                        {
                            cData = c;
                            seats = c.Capacity;
                        }
                    }
                    Movie mData = movieList.Find(x => x.Title == data[4]);

                    Screening newScreening = new Screening(i, Convert.ToDateTime(data[0]), data[1], cData, mData);
                    newScreening.SeatsRemaining = seats;
                    screeningList.Add(newScreening);
                    i++;
                }
            }
        }

        // FEATURE 3: LIST ALL MOVIES
        static void ListAllMovies(List<Movie> movieList)
        {
            Console.WriteLine("{0, 3} {1, -30} {2, 15} {3, -30} {4, -15} {5, -10}", "S/N", "Title", "Duration (mins)", "Genre", "Classification", "Opening Date");
            int sn = 1;
            foreach (Movie m in movieList)
            {
                string gnString = "";

                foreach (string genre in m.GenreList)
                {
                    if (genre != m.GenreList.Last())        // If chosen genre not last in array
                    {
                        gnString = gnString + genre + "/";
                    }
                    else
                    {
                        gnString = gnString + genre;
                    }
                }
                Console.WriteLine("{0, 3} {1, -30} {2, 15} {3, -30} {4, -15} {5, -10}", sn, m.Title, m.Duration, gnString, m.Classification, m.OpeningDate.ToString("dd/MM/yyyy"));
                sn++;
            }
        }

        // FEATURE 4: LIST MOVIE SCREENINGS
        static void ListMovieScreenings(List<Movie> movieList, List<Screening> screeningList)
        {
            ListAllMovies(movieList);
            // FEATURE 4.2: PROMPT USER TO SELECT A MOVIE
            int numOption;
            while (true)
            {
                Console.Write("Using the S/N, select a movie: ");
                string option = Console.ReadLine();
                bool isNumber = int.TryParse(option, out numOption);
                if (isNumber && numOption >= 1 && numOption <= movieList.Count)
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Invalid input. Please try again.");
                }
            }
            numOption--;

            // FEATURE 4.3: RETRIEVE MOVIE OBJECT
            Movie selectedMovie = movieList[numOption];

            // FEATURE 4.4: RETRIEVE AND DISPLAY SCREENING SESSIONS FOR THAT MOVIE
            Console.WriteLine();
            Console.WriteLine("Screening for {0}:", selectedMovie.Title);
            Console.WriteLine("Duration: {0} mins", selectedMovie.Duration);
            Console.WriteLine("Classification: {0}", selectedMovie.Classification);
            Console.WriteLine("Opening Date: {0}", selectedMovie.OpeningDate.ToString("dd/MM/yyyy"));
            Console.WriteLine();

            Console.WriteLine("{0, -4} {1, -25} {2, -5} {3, 15} {4, -15} {5, 7} {6, 10}",
                "No", "Date & Time", "Type", "Seats Remaining", "Cinema Name", "Hall No", "Capacity");
            foreach (Screening s in screeningList)
            {

                if (s.Movie == selectedMovie)
                {
                    Console.WriteLine("{0, -4} {1, -25} {2, -5} {3, 15} {4, -15} {5, 7} {6, 10}",
                        s.ScreeningNo, s.ScreeningDateTime, s.ScreeningType, s.SeatsRemaining, s.Cinema.Name, s.Cinema.HallNo, s.Cinema.Capacity);
                }
            }
        }

        // FEATURE 5: ADD A MOVIE SCREENING SESSION
        static void AddScreeningSession(List<Movie> movieList, List<Screening> screeningList, List<Cinema> cinemaList)
        {
            int sNo = screeningList.Last().ScreeningNo + 1;
            int numMovie;
            while (true)
            {
                Console.Write("Using the S/N, select a movie: ");
                string option = Console.ReadLine();
                bool isNumber = int.TryParse(option, out numMovie);
                if (isNumber && numMovie >= 1 && numMovie <= movieList.Count)
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Invalid input. Please try again.");
                }
            }
            numMovie--;
            Movie selectedMovie = movieList[numMovie];
            string sType;
            while (true)
            {
                Console.Write("Screening Type (2D/3D): ");
                sType = Console.ReadLine();
                string[] typeList = { "2D", "3D" };
                if (sType != null && typeList.Contains(sType.ToUpper()))
                {
                    sType = sType.ToUpper();
                    break;
                }
                else
                {
                    Console.WriteLine("Invalid input. Please try again.");
                }
            }
            DateTime newScreeningDT;
            while (true)
            {
                Console.Write("Enter screening date and time (dd/MM/yyyy HH:mm:ss): ");
                string inputDateTime = Console.ReadLine();
                string format = "dd/MM/yyyy HH:mm:ss";
                CultureInfo provider = new CultureInfo("en-SG");
                try
                {
                    newScreeningDT = DateTime.ParseExact(inputDateTime, format, provider);
                    if (newScreeningDT >= selectedMovie.OpeningDate)
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Error!! Screening date is before opening date!");
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Input not in the correct format. Please try again.");
                }
            }

            Console.WriteLine();
            int numCinema;
            Cinema selectedCinema = null;
            ListAllCinemas(cinemaList);
            while (true)
            {
                while (true)
                {
                    Console.Write("Select a cinema hall: ");
                    string option = Console.ReadLine();
                    bool isNumber = int.TryParse(option, out numCinema);
                    if (isNumber && numCinema >= 1 && numCinema <= cinemaList.Count)
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Invalid input. Please try again.");
                    }
                }
                numCinema--;
                selectedCinema = cinemaList[numCinema];
                bool cinemaEmpty = CheckCinemaHall(screeningList, newScreeningDT, selectedCinema, selectedMovie);
                if (cinemaEmpty)
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Movie screening session creation unsuccessful.");
                    Console.WriteLine("Cinema hall is not empty. Please enter another hall.");
                }
            }


            Screening newScreening = new Screening(sNo, newScreeningDT, sType, selectedCinema, selectedMovie);
            newScreening.SeatsRemaining = selectedCinema.Capacity;
            screeningList.Add(newScreening);
            Console.WriteLine("Movie screening session successfully created!");
            Console.WriteLine("Movie name: {0}", newScreening.Movie.Title);
            Console.WriteLine("No: {0}", newScreening.ScreeningNo);
            Console.WriteLine("Date & Time: {0}", newScreening.ScreeningDateTime);
            Console.WriteLine("Type: {0}", newScreening.ScreeningType);
            Console.WriteLine("Seats Remaining: {0}", newScreening.SeatsRemaining);
            Console.WriteLine("Cinema Hall: {0} / {1}", newScreening.Cinema.Name, newScreening.Cinema.HallNo);
            Console.WriteLine();
        }

        // FEATURE 5.5: LIST ALL CINEMA HALLS
        static void ListAllCinemas(List<Cinema> cinemaList)
        {
            int sn = 1;
            Console.WriteLine("{0, -4} {1, -15} {2, 7} {3, 10}",
                "No", "Cinema Name", "Hall No", "Capacity");
            foreach (Cinema c in cinemaList)
            {
                Console.WriteLine("{0, -4} {1, -15} {2, 7} {3, 10}",
                    sn, c.Name, c.HallNo, c.Capacity);
                sn++;
            }
        }

        // FEATURE 5.6: CHECKING IF CINEMA HALL IS AVAILABLE
        static bool CheckCinemaHall(List<Screening> screeningList, DateTime newScreeningDT, Cinema selectedCinema, Movie selectedMovie)
        {
            foreach (Screening s in screeningList)
            {
                if (s.Cinema == selectedCinema)
                {
                    DateTime sEndTime = s.ScreeningDateTime.AddMinutes(s.Movie.Duration + 30); // add 30 minutes
                    DateTime newMovieEndTime = newScreeningDT.AddMinutes(selectedMovie.Duration + 30); // add 30 mins for turnaround time
                    if (sEndTime >= newScreeningDT && newScreeningDT >= s.ScreeningDateTime)
                    {
                        return false; // occupied
                    }
                    else if (newMovieEndTime >= s.ScreeningDateTime && newScreeningDT <= s.ScreeningDateTime)
                    {
                        return false; // occupied
                    }
                }
            }
            return true;
        }

        // FEATURE 6: DELETE A MOVIE SCREENING SESSION
        static void DeleteScreeningSession(List<Screening> screeningList)
        {
            ListScreeningNoSales(screeningList);
            int sessionNo;
            Screening screening = null;
            while (true)
            {
                Console.Write("Select a session to remove: ");
                string option = Console.ReadLine();
                bool isNumber = int.TryParse(option, out sessionNo);
                if (isNumber)
                {
                    bool isSessionNo = false;
                    foreach (Screening s in screeningList)
                    {
                        if (sessionNo == s.ScreeningNo)
                        {
                            isSessionNo = true;
                            screening = s;
                            screeningList.Remove(screening);
                            Console.WriteLine("Removed successfully!");
                            return;
                        }
                    }
                    if (!isSessionNo)
                    {
                        Console.WriteLine("Removal unsuccessful.");
                        Console.WriteLine("Error! Input entered is not an existing screening session with no sales!");
                    }
                }
            }

            // FEATURE 6.1: LIST ALL MOVIE SCREENING SESSIONS THAT HAVE NOT SOLD ANY TICKETS
            static void ListScreeningNoSales(List<Screening> screeningList)
            {
                Console.WriteLine("Screening with no sales:");
                Console.WriteLine();
                Console.WriteLine("{0, -4} {1, -25} {2, -25} {3, -4} {4, -15} {5, -15}",
                    "No.", "Date & Time", "Movie Name", "Type", "Cinema Name", "Cinema Hall");
                foreach (Screening s in screeningList)
                {
                    if (s.SeatsRemaining == s.Cinema.Capacity)
                    {
                        Console.WriteLine("{0, -4} {1, -25} {2, -25} {3, -4} {4, -15} {5, -15}",
                            s.ScreeningNo, s.ScreeningDateTime, s.Movie.Title, s.ScreeningType, s.Cinema.Name,
                            s.Cinema.HallNo);
                    }
                }
            }
        }

        // FEATURE 7: ORDER MOVIE TICKETS
        static void OrderMovieTickets(List<Movie> movieList, List<Screening> screeningList, int orderNo, List<Order> orderList)
        {
            // FEATURE 7.1: LIST ALL MOVIES
            ListAllMovies(movieList);

            // FEATURE 7.2: PROMPT USER TO SELECT A MOVIE
            int inputMovie;
            Movie orderMovie = null;
            while (true)
            {
                Console.Write("Using the S/N, select a movie: ");
                string option = Console.ReadLine();
                bool isNumber = int.TryParse(option, out inputMovie);
                if (isNumber && inputMovie >= 1 && inputMovie <= movieList.Count)
                {
                    orderMovie = movieList[--inputMovie];
                    break;
                }
                else
                {
                    Console.WriteLine("Invalid input. Please try again.");
                }
            }

            // FEATURE 7.3: LIST ALL MOVIE SCREENINGS OF THE SELECTED MOVIE
            Console.WriteLine(">> Screenings for {0}:", orderMovie.Title);
            Console.WriteLine("{0, -4} {1, -25} {2, -5} {3, 15} {4, -15} {5, 7} {6, 10}",
                "No", "Date & Time", "Type", "Seats Remaining", "Cinema Name", "Hall No", "Capacity");

            foreach (Screening s in screeningList)
            {
                if (s.Movie == orderMovie)
                {
                    Console.WriteLine("{0, -4} {1, -25} {2, -5} {3, 15} {4, -15} {5, 7} {6, 10}",
                        s.ScreeningNo, s.ScreeningDateTime, s.ScreeningType, s.SeatsRemaining, s.Cinema.Name, s.Cinema.HallNo, s.Cinema.Capacity);
                }
            }

            // FEATURE 7.4: PROMPT USER TO SELECT MOVIE SCREENING
            int inputScreening;
            Screening orderScreening = null;
            while (true)
            {
                Console.Write("Using the Screening No, select a screening: ");
                string option = Console.ReadLine();
                bool isNumber = int.TryParse(option, out inputScreening);
                bool isMovie = false;
                if (isNumber)
                {
                    // FEATURE 7.5: RETRIEVE THE SELECTED MOVIE SCREENING
                    foreach (Screening s in screeningList)
                    {
                        if (s.Movie == orderMovie && s.ScreeningNo == inputScreening)
                        {
                            orderScreening = s;
                            isMovie = true;
                            break;
                        }
                    }
                    if (isMovie)
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Screening number does not exist!");
                    }
                }
                else
                {
                    Console.WriteLine("Error! You did not enter a number!");
                }

            }

            // FEATURE 7.6: PROMPT USER FOR TOTAL NUMBER OF TICKETS TO ORDER
            int numOption;
            int totalTickets;
            while (true)
            {
                Console.Write("Enter total number of tickets to order: ");
                string option = Console.ReadLine();
                bool isNumber = int.TryParse(option, out numOption);
                if (isNumber && numOption <= orderScreening.SeatsRemaining)
                {
                    totalTickets = numOption;
                    break;
                }
                else
                {
                    Console.WriteLine("Invalid input. Please try again.");
                }
            }

            bool classificationConfirmation = false;
            // FEATURE 7.7: PROMPT USER IF ALL THE TICKET HOLDERS MEET THE MOVIE CLASSIFICATION REQUIREMENTS
            if (orderMovie.Classification == "PG13")
            {
                Console.Write("Are all the ticket holders aged 13 and above? (Enter 'Yes' or 'No') ");
                classificationConfirmation = CheckAgeLimit();
            }
            else if (orderMovie.Classification == "NC16")
            {
                Console.Write("Are all the ticket holders aged 16 and above? (Enter 'Yes' or 'No') ");
                classificationConfirmation = CheckAgeLimit();
            }
            else if (orderMovie.Classification == "M18")
            {
                Console.Write("Are all the ticket holders aged 18 and above? (Enter 'Yes' or 'No') ");
                classificationConfirmation = CheckAgeLimit();
            }
            else if (orderMovie.Classification == "R21")
            {
                Console.Write("Are all the ticket holders aged 21 and above? (Enter 'Yes' or 'No') ");
                classificationConfirmation = CheckAgeLimit();
            }
            else
            {
                classificationConfirmation = true;
            }



            if (classificationConfirmation)
            {
                // FEATURE 7.8: CREATE AN ORDER OBJECT WITH THE STATUS "UNPAID"
                orderNo++;
                Order newOrder = new Order(orderNo, DateTime.Now);
                newOrder.Status = "Unpaid";

                // FEATURE 7.9: CREATED TICKETS FOR NEW ORDER
                for (int i = 0; i < totalTickets; i++)
                {
                    // ADVANCED FEATURE 3.3: PROMPT USER TO CHOOSE SEATS
                    PrintSeatingPlan(orderScreening.Cinema);
                    List<char> alphabetArr = new List<char> { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
                    Console.WriteLine("Ticket {0}", i + 1);
                    string chosenSeatNo = null;
                    while (true)
                    {
                        Console.Write("Select your seat (eg. A1): ");
                        chosenSeatNo = Console.ReadLine().ToUpper();
                        bool seatAvail = false;
                        if (chosenSeatNo.Length == 2)
                        {
                            foreach (char ch in alphabetArr)
                            {
                                int seatCol = Convert.ToInt32(chosenSeatNo[1]);
                                if (chosenSeatNo[0] == ch && (49 <= seatCol && seatCol <= 53)) // DECIMAL 49 to 53 == CHAR 1 to 5
                                {

                                    seatAvail = true;
                                    break;
                                }
                                else
                                {
                                    seatAvail = false;
                                }
                            }
                            if (!seatAvail)
                            {
                                Console.WriteLine("Seat does not exist. Try again.");
                            }

                        }
                        else
                        {
                            Console.WriteLine("Seat does not exist. Try again.");
                        }
                        if (seatAvail)
                        {
                            break;
                        }

                    }

                    Console.Write("Enter type of ticket ordered for ticket {0} (eg. Adult, Student, SeniorCitizen): ", i + 1);
                    string ticketType = Console.ReadLine();

                    if (ticketType.ToLower() == "student")
                    {
                        while (true)
                        {
                            Console.Write("What is your level of study? (eg. Primary, Secondary, Tertiary) ");
                            string levelOfStudyResponse = Console.ReadLine().ToLower();
                            if (levelOfStudyResponse == "primary" || levelOfStudyResponse == "secondary" || levelOfStudyResponse == "tertiary")
                            {
                                Ticket newTicket = new Student(orderScreening, chosenSeatNo, levelOfStudyResponse);
                                newOrder.TicketList.Add(newTicket);
                                orderScreening.SeatsRemaining--;
                                break;
                            }
                            else
                            {
                                Console.WriteLine("Wrong input entered.");
                            }

                        }


                    }
                    else if (ticketType.ToLower() == "seniorcitizen")
                    {
                        Console.Write("Which year were you born in? ");
                        int yearOfBirthResponse = Convert.ToInt32(Console.ReadLine());
                        int age = DateTime.Now.Year - yearOfBirthResponse;
                        if (age >= 55)
                        {
                            Ticket newTicket = new SeniorCitizen(orderScreening, chosenSeatNo, yearOfBirthResponse);
                            newOrder.TicketList.Add(newTicket);
                            orderScreening.SeatsRemaining--;
                        }
                        else
                        {
                            Console.WriteLine("You're too young to be a Senior Citizen!");
                        }
                    }
                    else // By default, an adult ticket is to be purchased
                    {
                        Ticket newTicket = null;
                        Console.Write("Would you like to get a popcorn for $3? (Enter 'Y' or 'N') ");
                        string popcornOfferResponse = Console.ReadLine();
                        if (popcornOfferResponse.ToLower() == "y")
                        {
                            newTicket = new Adult(orderScreening, chosenSeatNo, true);

                        }
                        else
                        {
                            newTicket = new Adult(orderScreening, chosenSeatNo, false);
                        }

                        newOrder.TicketList.Add(newTicket);
                        orderScreening.SeatsRemaining--;
                    }
                }

                // FEATURE 7.10: LIST AMOUNT PAYABLE
                double amountPayable = 0.00;
                for (int i = 0; i < newOrder.TicketList.Count(); i++)
                {
                    amountPayable += newOrder.TicketList[i].CalculatePrice();
                }
                Console.WriteLine("Total amount payable: ${0:0.00}", amountPayable);

                // FEATURE 7.11: PROMPT USER TO PRESS ANY KEY TO MAKE PAYMENT
                Console.Write("Press any key to make payment ...");
                Console.ReadKey();
                Console.WriteLine();
                Console.WriteLine();

                // FEATURE 7.12: FILL IN THE NECESSARY DETAILS FOR THE NEW ORDER
                newOrder.Amount = amountPayable;

                // FEATURE 7.13: CHANGE ORDER STATUS TO "PAID"
                newOrder.Status = "Paid";
                Console.WriteLine("Order Summary:");
                Console.WriteLine("Order no.: {0}", newOrder.OrderNo);
                Console.WriteLine("{0, -12}{1, -10}{2, -15}{3, -15}", "Ticket No.", "Seat No.", "Ticket Type", "Price (including add-ons eg. Popcorn Offer)");
                for (int i = 0; i < newOrder.TicketList.Count; i++)
                {
                    Console.WriteLine("{0, -12}{1, -10}{2, -15}{3, -15}", i + 1, newOrder.TicketList[i].SeatNo, newOrder.TicketList[i].GetType().Name, newOrder.TicketList[i].CalculatePrice());
                }
                orderList.Add(newOrder);
            }
            else
            {
                Console.WriteLine("You may not purchase tickets for this movie as not all ticket holders reached the required age limit.");
            }
        }

        // FEATURE 7.7: CHECK AGE LIMIT
        static bool CheckAgeLimit()
        {
            while (true)
            {
                string response = Console.ReadLine().ToLower();

                if (response == "yes")
                {
                    return true;
                }
                else if (response == "no")
                {
                    return false;
                }
                else
                {
                    Console.WriteLine("Please enter \"Yes\" or \"No\".");
                }
            }

        }

        // FEATURE 8: CANCEL MOVIE TICKETS
        static void CancelMovieTickets(List<Order> orderList)
        {
            // FEATURE 8.1: PROMPT USER FOR ORDER NUMBER
            while (true)
            {
                Console.Write("Enter the order no. of the order you would like to cancel: ");
                int orderNoToCancel = Convert.ToInt32(Console.ReadLine());
                Order orderToCancel = orderList.Find(order => order.OrderNo == orderNoToCancel);
                {
                    if (orderToCancel != null)
                    {
                        if (orderToCancel.TicketList[0].Screening.ScreeningDateTime > DateTime.Now) // Checks if tickets expired
                        {
                            foreach (Ticket t in orderToCancel.TicketList)
                            {
                                t.Screening.SeatsRemaining++;
                            }
                            orderToCancel.Status = "Cancelled";
                            Console.WriteLine("Your order has been refunded.");
                            Console.WriteLine("Your cancellation was successful.");
                            return;
                        }
                        else
                        {
                            Console.WriteLine("Error! Your movie tickets have expired!");
                            Console.WriteLine("Your cancellation was unsuccessful.");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Error! You did not enter an existing order no.!");
                        Console.WriteLine("Your cancellation was unsuccessful.");
                    }
                }
            }
        }

        // ADVANCED FEATURE 3.1: DISPLAY AVAILABLE SEATS OF SCREENING SESSION IN DESCENDING ORDER
        static void ScreeningAvailableSeats(List<Screening> screeningList)
        {
            List<Screening> sortedScreeningList = screeningList.OrderBy(x => x.SeatsRemaining).ToList();
            sortedScreeningList.Reverse();

            Console.WriteLine("{0, -4} {1, -25} {2, -30} {3, -5} {4, 15} {5, -15} {6, 7}",
                "No", "Date & Time", "Title", "Type", "Seats Remaining", "Cinema Name", "Hall No");
            foreach (Screening s in sortedScreeningList)
            {
                Console.WriteLine("{0, -4} {1, -25} {2, -30} {3, -5} {4, 15} {5, -15} {6, 7}",
                        s.ScreeningNo, s.ScreeningDateTime, s.Movie.Title, s.ScreeningType, s.SeatsRemaining, s.Cinema.Name, s.Cinema.HallNo);
            }
        }

        // ADVANCED FEATURE 3.3: PRINT SEATING PLAN FOR THE DIFFERENT CINEMAS
        static void PrintSeatingPlan(Cinema cinema)
        {
            char[] alphabetArr = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
            Console.WriteLine();
            Console.WriteLine("Seating Plan");
            for (int i = 0; i < cinema.Capacity / 5; i++) // Creates x number of rows of 5 columns of seats, where x = cinema capacity divided by 5
            {
                Console.WriteLine("{0}  1   2   3   4   5", alphabetArr[i]);
            }

        }
    }
}