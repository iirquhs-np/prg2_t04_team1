﻿// ============================================================
// Student Number : S10222764, S10227932
// Student Name : Xiao Yaojie, Shuqri Bin Shaifuddin
// Module Group : T04
// ============================================================

using System;
using System.Collections.Generic;

namespace prg2_t04_team1
{
    public class Order
    {
        // Properties
        public int OrderNo { get; set; }
        public DateTime OrderDateTime { get; set; }
        public double Amount { get; set; }
        public string Status { get; set; }
        public List<Ticket> TicketList { get; set; }
        
        // Constructors
        public Order(){}
        public Order(int on, DateTime odt)
        {
            OrderNo = on;
            OrderDateTime = odt;
            TicketList = new List<Ticket>();
        }

        // Methods
        public void AddTicket(Ticket t)
        {
            TicketList.Add(t);
        }

        public override string ToString()
        {
            return "OrderNo: " + OrderNo + "\tOrderDateTime: " + OrderDateTime + "\tAmount: " + Amount + "\tStatus: " + Status;
        }
    }
}